;; [[file:~/.emacs.d/config.org::*Repos][Repos:1]]
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
;; Repos:1 ends here

;; [[file:~/.emacs.d/config.org::*Emacs%20Main%20Configs][Emacs Main Configs:1]]
(setq inhibit-startup-message t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)
(global-linum-mode 1)
(toggle-frame-maximized)
(setq custom-file (make-temp-file "emacs-custom")) ;; Avoid write trash on config files
;; Emacs Main Configs:1 ends here

;; [[file:~/.emacs.d/config.org::*Emacs%20Lang%20UTF][Emacs Lang UTF:1]]
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; Emacs Lang UTF:1 ends here

;; [[file:~/.emacs.d/config.org::*Emacs%20Keymapping][Emacs Keymapping:1]]
(global-set-key (kbd "<f5>") 'revert-buffer)
(global-set-key (kbd "C-z") 'undo)
(global-set-key (kbd "C-v") 'clipboard-yank) 
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <left>") 'windmove-left)
(global-set-key (kbd "C-c <up") 'windmove-up)
(global-set-key (kbd "C-c <down>") 'windmove-down)

(global-set-key (kbd "C-c h") 'split-window-horizontally)
(global-set-key (kbd "C-c v") 'split-window-vertically)

(global-set-key (kbd "C-c i") (lambda () (interactive)(find-file "~/.emacs.d/config.org")))
(global-set-key (kbd "C-c t") 'open-task)
;; Emacs Keymapping:1 ends here

;; [[file:~/.emacs.d/config.org::*Emacs%20Extra%20Functions][Emacs Extra Functions:1]]
(defun new-empty-buffer ()
  "Create a new empty buffer.
New buffer will be named “untitled” or “untitled<2>”, “untitled<3>”, etc.

It returns the buffer (for elisp programing).

URL `http://ergoemacs.org/emacs/emacs_new_empty_buffer.html'
Version 2017-11-01"
  (interactive)
  (let (($buf (generate-new-buffer "untitled")))
    (switch-to-buffer $buf)
    (funcall initial-major-mode)
    (setq buffer-offer-save t)
    $buf))

(defun open-task ()
  "Load TASKS.org file in a splitted window"
  (interactive)
  (split-window-horizontally) 
  (other-window 1)
  (find-file "~/org/tasks.org"))
;; Emacs Extra Functions:1 ends here

;; [[file:~/.emacs.d/config.org::*TabBar][TabBar:1]]
(use-package tabbar
  :ensure t
  :config
  (setq tabbar-use-images nil)
 )
;; TabBar:1 ends here

;; [[file:~/.emacs.d/config.org::*][No heading:1]]
(use-package neotree
  :ensure t
  :bind (
    ("<f8>" . neotree-toggle)
  )
  :config
  (setq neo-smart-open t)
  (setq neo-vc-integration nil)
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  (setq inhibit-compacting-font-caches t)
)
;; No heading:1 ends here

;; [[file:~/.emacs.d/config.org::*All-the-icons%20and%20Fonts][All-the-icons and Fonts:1]]
(use-package all-the-icons
  :ensure t
  ;; :init (all-the-icons-install-fonts t)
)
;; All-the-icons and Fonts:1 ends here

;; [[file:~/.emacs.d/config.org::*Theme:%20doom-theme][Theme: doom-theme:1]]
(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-dark+ t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-one") ; use the colorful treemacs theme
  (doom-themes-treemacs-config)
  
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))
;; Theme: doom-theme:1 ends here

;; [[file:~/.emacs.d/config.org::*Projectile][Projectile:1]]
(use-package projectile
  :ensure t
  :config
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))
;; Projectile:1 ends here

;; [[file:~/.emacs.d/config.org::*Emacs%20Dashboard][Emacs Dashboard:1]]
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (with-eval-after-load 'dashboard-mode
   (global-linum-mode 0)) ;; Turnoff line number on dashboard
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-banner-logo-title "")
  (setq dashboard-init-info "Happy Hacking!\n")
  (setq dashboard-items '((recents  . 5)
                          (projects . 5)))
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-init-info t)
  (setq dashboard-set-navigator t)
  (setq dashboard-navigator-buttons
    `(;; line1
      (
       ("" "New File" "Create an empty file"
        (lambda (&rest _) (new-empty-buffer))
        error "[ " " ]")
       ("" "Tasks" "Manage Tasks"
	  (lambda (&rest _) (interactive)(find-file "~/org/tasks.org"))
        error "[ " " ]"))
     )))
;; Emacs Dashboard:1 ends here

;; [[file:~/.emacs.d/config.org::*Company%20-%20Autocomplete][Company - Autocomplete:1]]
(use-package company
  :ensure t
  :init
  (global-company-mode)
  (company-complete)
  (company-tng-configure-default) ;; Use TAB to navigate between suggestions
  (setq company-selection-wrap-around t) ;; Cycle suggestion list
   :bind(
    ("C-SPC" . company-complete-selection)))
;; Company - Autocomplete:1 ends here

;; [[file:~/.emacs.d/config.org::*Org][Org:1]]
(use-package org
  :bind (
  ("C-c l" . org-store-link)
  ("C-c a" . org-agenda)
  ("C-c c" . org-capture)
  )

 :config
  (setq org-startup-indented 'f)
  (setq org-directory "~/org")
  (setq org-special-ctrl-a/e 't)
  (setq org-default-notes-file (concat org-directory "/notes.org"))
  (setq org-src-fontify-natively 't)
  (setq org-src-tab-acts-natively t)
  (setq org-src-window-setup 'current-window)
  (setq org-todo-keywords
      '(
        (sequence "TODO(t)" "STARTED(s)" "ON HOLD(h)" "|" "DONE(d)")
        (sequence "|" "CANCELLED(c)" "DELEGATED(l)" "SOMEDAY(f)")
        (seqeunce "|" "IDEA(i)")
        ))

  (setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("STARTED" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("ON HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
              ("DELEGATED" :foreground "forest green" :weight bold)
              ("SOMEDAY" :foreground "forest green" :weight bold)
              ("IDEA" :foreground "yellow" :weight bold))))

)
;; Org:1 ends here

;; [[file:~/.emacs.d/config.org::*Smartparens][Smartparens:1]]
(use-package smartparens-config
  :ensure smartparens
  :bind (
    ("C-<right>" . sp-forward-slurp-sexp)
    ("M-<right>" . sp-forward-barf-sexp)
    ("C-<left>"  . sp-backward-slurp-sexp)
    ("M-<left>"  . sp-backward-barf-sexp)
  )
  :config (progn (show-smartparens-global-mode t)))

(add-hook 'prog-mode-hook 'turn-on-smartparens-strict-mode)
(add-hook 'markdown-mode-hook 'turn-on-smartparens-strict-mode)
;; Smartparens:1 ends here
